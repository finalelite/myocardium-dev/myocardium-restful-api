'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with accounts
 */

const Account = use('App/Models/Account')

class AccountController {
	/**
	 * Show a list of all accounts.
	 * GET accounts
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({request, response, view}) {
		return await Account.all()
	}

	/**
	 * Create/save a new account.
	 * POST accounts
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({request, response}) {
		const data = request.only(['nickname'])
		data.groups = 1
		return await Account.create(data)
	}

	/**
	 * Display a single account.
	 * GET accounts/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({params, request, response, view}) {
		return await Account.findBy({'nickname': params.id})
	}

	/**
	 * Update account details.
	 * PUT or PATCH accounts/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({params, request, response}) {
		const account = await Account.findBy({'nickname': params.id})
		const data = request.only(['groups', 'discord_id'])

		try {
			account.merge(data)
			await account.save()
		} catch (error) {
			return await Account.findBy({'nickname': params.id})
		}


		return await Account.findBy({'nickname': params.id})


	}

	/**
	 * Delete a account with id.
	 * DELETE accounts/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({params, request, response}) {
		const account = await Account.findBy({'nickname': params.id})
		try {
			await account.delete()
		} catch (error) {
		}
	}
}

module
	.exports = AccountController
