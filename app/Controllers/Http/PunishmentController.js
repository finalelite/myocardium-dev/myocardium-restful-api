'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with punishments
 */

const Punishment = use('App/Models/Punishment')

class PunishmentController {

	/**
	 * Show a list of all punishments.
	 * GET punishments
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {
		return await Punishment.all()
	}

	/**
	 * Create/save a new punishment.
	 * POST punishments
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const data = request.only(['player', 'reason', 'author', 'type', 'duration'])
		data.revoked = false
		data.time = Date.now()
		const newPunishment = await Punishment.create(data)
		return await Punishment.findBy({'id':newPunishment.id})
	}

	/**
	 * Display a list of punishments.
	 * GET punishments/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
		return await Punishment.query().where('player', params.id).fetch()
	}

	/**
	 * Update punishment details.
	 * PUT or PATCH punishments/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
		const punishment = await Punishment.findBy({'id':params.id})
		const data = request.only(['revoke_reason', 'revoke_author'])
		data.revoked = true
		data.revoke_time = Date.now()

		punishment.merge(data)
		await punishment.save()

		return await Punishment.findBy({'id':params.id})

	}

	/**
	 * Delete a punishment with id.
	 * DELETE punishments/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
		const punishment = await Punishment.findBy({'id':params.id})
		try {
			await punishment.delete()
		} catch (error) {
		}
	}
}

module.exports = PunishmentController
