# Myocardium Restful API (MRA)

Essa é a API Restful para o sistema de integração Myocardium (MRA). Todas as chamadas
aos bancos de dados devem ser realizadas por meio dessa API.

---

## Depedências

MRA requer as seguintes depedências para funcionar corretamente.

- [NodeJS](https://nodejs.org/en/)
- [npm](https://www.npmjs.com/)
- [AdonisJS](https://adonisjs.com)


## Como instalar e executar

- Clone esse repositório em sua máquina local
- Execute `npm install` para instalar todas as dependências
- Faça uma cópia de `.env.example` e renomeie-a para `.env`
- Execute `adonis key:generate` para gerar sua chave secreta
- Execute `adonis migration:run` para configurar o banco de dados.
- Execute `adonis serve --dev` para finalmente executar a aplicação.

## Rotas

Chamadas aos Bancos de Dados são efetuadas por meio de rotas, cada rota com uma função em específico.

### Account
##### A rota Account faz chamadas referentes a contas dos jogadores.

---
```http request
Mostra uma lista de todas as contas:

GET /account


Response:
[
  {
    "id": 2,
    "nickname": "Marcosen",
    "groups": 1,
    "created_at": "2018-12-25 02:54:16",
    "updated_at": "2018-12-25 02:54:16"
  },
  {
    "id": 3,
    "nickname": "JohnDoe",
    "groups": 1,
    "created_at": "2018-12-25 02:54:27",
    "updated_at": "2018-12-25 02:54:27"
  }
]
```
---
```http request
Mostra uma única conta:

GET /account/JohnDoe


Response:
{
  "id": 3,
  "nickname": "JohnDoe",
  "groups": 1,
  "created_at": "2018-12-25 02:54:27",
  "updated_at": "2018-12-25 02:54:27"
}
```
---
```http request
Cria uma nova conta (usada quando um novo jogador entra no servidor):

POST /account
Content-Type: application/json
{
  "nickname": "JohnDoe"
}


Response:
{
  "nickname": "JohnDoe",
  "groups": 1,
  "created_at": "2018-12-25 02:54:27",
  "updated_at": "2018-12-25 02:54:27",
  "id": 3
}

```
---
```http request
Atualiza os grupos da conta:

PUT /account/JohnDoe
Content-Type: application/json
{
  "groups": 1024
}

Response:
{
  "id": 3,
  "nickname": "JohnDoe",
  "groups": 1024,
  "created_at": "2018-12-25 02:54:27",
  "updated_at": "2018-12-25 02:55:52"
}
```

### Punishment
##### A rota Punishment faz chamadas relacionadas ao sistema de punições in-game.

---
```http request
Mostra todas as punições

GET /punishment

Response:
[
  {
    "id": 1,
    "player": "JohnDoe",
    "reason": "Uso de Trapaças",
    "author": "Insomnia",
    "type": "BAN",
    "duration": -1,
    "revoked": 1,
    "revoke_reason": "Comprou perdão no site.",
    "revoke_author": "Loja Online",
    "time": 1545849224812,
    "revoke_time": 1545854026526,
    "created_at": "2018-12-26 16:33:44",
    "updated_at": "2018-12-26 17:53:46"
  },
  {
    "id": 2,
    "player": "JohnDoe",
    "reason": "Violações das Regras da Comunidade",
    "author": "Insomnia",
    "type": "MUTE",
    "duration": -1,
    "revoked": 0,
    "revoke_reason": null,
    "revoke_author": null,
    "time": 1545850892838,
    "revoke_time": null,
    "created_at": "2018-12-26 17:01:32",
    "updated_at": "2018-12-26 17:01:32"
  }
]
```
---
```http request
Mostra todas as punições de um jogador

GET /punishment/JohnDoe

Response:
[
  {
    "id": 1,
    "player": "JohnDoe",
    "reason": "Uso de Trapaças",
    "author": "Insomnia",
    "type": "BAN",
    "duration": -1,
    "revoked": 1,
    "revoke_reason": "Comprou perdão no site.",
    "revoke_author": "Loja Online",
    "time": 1545849224812,
    "revoke_time": 1545854026526,
    "created_at": "2018-12-26 16:33:44",
    "updated_at": "2018-12-26 17:53:46"
  },
  {
    "id": 2,
    "player": "JohnDoe",
    "reason": "Violações das Regras da Comunidade",
    "author": "Insomnia",
    "type": "MUTE",
    "duration": -1,
    "revoked": 0,
    "revoke_reason": null,
    "revoke_author": null,
    "time": 1545850892838,
    "revoke_time": null,
    "created_at": "2018-12-26 17:01:32",
    "updated_at": "2018-12-26 17:01:32"
  }
]
```
---
```http request
Cria uma nova punição

POST /punishment
Content-Type: application/json
{
  "player": "JohnDoe",
  "reason": "Violações das Regras da Comunidade",
  "author": "Insomnia",
  "type": "MUTE",
  "duration": -1
 }
 
 Response:
 {
   "id": 2,
   "player": "JohnDoe",
   "reason": "Violações das Regras da Comunidade",
   "author": "Insomnia",
   "type": "MUTE",
   "duration": -1,
   "revoked": 0,
   "revoke_reason": null,
   "revoke_author": null,
   "time": 1545850892838,
   "revoke_time": null,
   "created_at": "2018-12-26 17:01:32",
   "updated_at": "2018-12-26 17:01:32"
 }
```
---
```http request
Revoga uma punição, anulando os seus efeitos

PUT /punishment/1
Content-Type: application/json
{
  "revoke_reason": "Comprou perdão no site.",
  "revoke_author": "Loja Online"
}

Response:
{
  "id": 1,
  "player": "JohnDoe",
  "reason": "Uso de Trapaças",
  "author": "Insomnia",
  "type": "BAN",
  "duration": -1,
  "revoked": 1,
  "revoke_reason": "Comprou perdão no site.",
  "revoke_author": "Loja Online",
  "time": 1545849224812,
  "revoke_time": 1545854026526,
  "created_at": "2018-12-26 16:33:44",
  "updated_at": "2018-12-26 17:53:46"
}
```
---




