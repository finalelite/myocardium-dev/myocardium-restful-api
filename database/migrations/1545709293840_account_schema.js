'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AccountSchema extends Schema {
	up() {
		this.create('accounts', (table) => {
			table.increments()
			table.string('nickname').notNullable().unique()
			table.bigInteger('groups').notNullable()
			table.string('discord_id').unique()
			table.timestamps()
		})
	}

	down() {
		this.drop('accounts')
	}
}

module.exports = AccountSchema
