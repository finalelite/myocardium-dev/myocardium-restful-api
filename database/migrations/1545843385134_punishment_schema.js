'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PunishmentSchema extends Schema {
  up () {
    this.create('punishments', (table) => {
      table.increments()
      table.string('player').notNullable()
      table.string('reason').notNullable()
      table.string('author').notNullable()
      table.string('type').notNullable()
      table.bigInteger('duration').notNullable()
      table.boolean('revoked')
      table.string('revoke_reason')
      table.string('revoke_author')
      table.bigInteger('time')
      table.bigInteger('revoke_time')
      table.timestamps()
    })
  }

  down () {
    this.drop('punishments')
  }
}

module.exports = PunishmentSchema
